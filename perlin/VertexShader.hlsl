RWTexture2D<uint> uav1 : register(u0);
//RWTexture2D<uint> DynamicBuffer : register(u1);

struct VS_INPUT
{
	float3 pos : POSITION;
	float3 normal: NORMAL;
	float2 texcoords: TEXCOORDS;
};

struct VS_OUTPUT
{
	float4 pos: SV_POSITION;
	float3 normal: NORMAL;
	float2 texcoords : TEXCOORDS;
};

cbuffer ConstantBuffer: register (b0)
{
	float4x4 wvpMat;
};

cbuffer ConstantBuffer: register (b1)
{
	uint mut;
};

int p(int l)
{
	//access the random texture
	return uav1.Load(int2(l - floor(l / 100.f)*100.f, floor(l / 100.f))).x;
}

//Randomly selecting directions. This is from an online tutorial
int2 grad(int hash) {
	switch (hash & 0xF)
	{
	case 0x0: return int2(1, 1);
	case 0x1: return int2(-1, 1);
	case 0x2: return int2(1, -1);
	case 0x3: return int2(-1, -1);
	case 0x4: return int2(1, 1);
	case 0x5: return int2(-1, 1);
	case 0x6: return int2(1, -1);
	case 0x7: return int2(-1, -1);
	case 0x8: return int2(1, 1);
	case 0x9: return int2(-1, 1);
	case 0xA: return int2(1, -1);
	case 0xB: return int2(-1, -1);
	case 0xC: return int2(1, 1);
	case 0xD: return int2(-1, 1);
	case 0xE: return int2(1, -1);
	case 0xF: return int2(-1, -1);
	default: return int2(0, 0); // never happens
	}
}

float fade(float t) {
	// Fade function as defined by Ken Perlin.  This eases coordinate values
	// so that they will ease towards integral values.  This ends up smoothing
	// the final output.
	return  t * t * t * (t * (t * 6.f - 15.f) + 10.f);         // 6t^5 - 15t^4 + 10t^3
}
float lerps(float a, float b, float x) {
	return a + x * (b - a);
}

float perlin(int x, int y, uint freq) {
	int xi = x / freq; //Goes from 0 to 10 -> certified top -> down
	int yi = y / freq; //From 0 to 10 left -> right

	//map to random-texture space 0-100, 0-100:
	int xip = xi * freq; //back to map from 0-100
	int yip = yi * freq;

	//Grab random value based on xpi in the rng texture
	int rnd1 = p(p(xip) + yip);
	int rnd2 = p(p(xip) + yip + freq);
	//calculate a random gradient vector for every grid cell (row) using the rng hash:
	int2 rvec = grad(rnd1);
	int2 rvec2 = grad(rnd2);
	//Get coordinate of sample inside cell
	float xf = x / (float)freq - xi;
	float yf = y / (float)freq - yi;

	//Get (negtaive) coordinate of sample towards next cell
	float xfn = xf - 1;
	float yfn = yf - 1;
	//Get gradient of next cell:
	int rndn = p(p(xip + freq) + yip);
	int rndn2 = p(p(xip + freq) + yip + freq);
	int2 rvecn = grad(rndn);
	int2 rvecn2 = grad(rndn2);

	//calc dot product between position in cell and gradient of cell
	float dotp = xf * rvec.x + yf * rvec.y;
	float dotpn = xfn * rvecn.x + yf * rvecn.y;

	float dotp2 = xf * rvec2.x + yfn * rvec2.y;
	float dotpn2 = xfn * rvecn2.x + yfn * rvecn2.y;

	//smoothly run from one vector to the next
	float val1 = lerp(dotp, dotpn, fade(xf));
	float val2 = lerp(dotp2, dotpn2, fade(xf));
	float val3 = lerp(val1, val2, fade(yf));

	return (val3 + 1) / 2.f;

}

float octave(int x, int y, int octaves)
{
	float final = 0;
	float amp = 1;
	float freq = 20;
	float change = 0.45;
	float maxValue = 0;  // Used for normalizing result to 0.0 - 1.0
	for (int i = 0; i < octaves; i++) {
		final += perlin(x, y, freq) * amp;
		maxValue += amp;
		amp *= change;
		freq /= 2;
	}

	return final / maxValue;
}
/*
This Perlin Vertex Shader is based on the tutorial found here:
http://flafla2.github.io/2014/08/09/perlinnoise.html
I rewrote it partially to adapt to the vertex shader.
And yes, the branching is not really optimal for a shader, but for a small
demo it works quite well.
*/
VS_OUTPUT main(VS_INPUT input)
{
	VS_OUTPUT output;

	//Generate Terrain
	float height = octave((input.normal[0]), (input.normal[2] + mut), 4);
	//Flatten the water
	float water = 0.0f;
	if (height < 0.3) {
		height = 0.3;
		water = 1;
	}
	//Scale height by an arbitrary number, here it is 20
	float4 pos = float4(input.pos[0], height * 20, float(input.pos[2]), 1.0f);
	output.pos = mul(pos, wvpMat);

	//Set blending factors for the remaining two terrain types (other than water)
	float mountain = (1 - water)*max(0, lerp(0, 1, max(0, height * 2 - 0.6)));
	float grass = (1 - water)*max(0, lerp(1, -0.75, height));

	//Normal parameter are used for blending here. They dont actually contain normal information. I could rename them but i want to keep the names for later light computation
	output.normal = float3(mountain, grass, water);
	//Pass on texcoords. Y coords are modified to scroll with noise texture. With that it appears as if the terrain is moving.
	output.texcoords = float2(input.texcoords.x, input.texcoords.y + mut / 100.f);
	return output;
}