#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN    // Exclude rarely-used stuff from Windows headers.
#endif
#include <windows.h>
#include <d3d12.h>
#include <dxgi1_4.h>
#include <D3Dcompiler.h>
#include <DirectXMath.h>
#include "d3dx12.h"
#include <string>

using namespace DirectX;

// this will only call release if an object exists (prevents exceptions calling release on non existant objects)
#define SAFE_RELEASE(p) { if ( (p) ) { (p)->Release(); (p) = 0; } }

struct Vertex {
	Vertex(float x, float y, float z, float nx, float ny, float nz, float u, float v) : pos(x, y, z), normal(nx, ny, nz), texCoords(u, v) {}
	XMFLOAT3 pos;
	XMFLOAT3 normal;
	XMFLOAT2 texCoords;
};

struct CameraBuffer {
	XMFLOAT4X4 wvpMat;
};
int ConstantBufferPerObjectAlignedSize = (sizeof(CameraBuffer) + 255) & ~255;

//Functions:
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
bool InitializeWindow(HINSTANCE hInstance, int ShowWnd, bool fullscreen);
bool InitD3D(); // initializes direct3d 12
bool InitGeometry();
void Update(); // update the game logic
void UpdatePipeline(); // update the direct3d pipeline (update command lists)
void Render(); // execute the command list
void Cleanup(); // release com ojects and clean up memory
void WaitForPreviousFrame(); // wait until gpu is finished with command list direct3d stuff
void mainloop(); //The mainloop
bool InitViewport(); //Init viewport
bool InitPSO(); //Init Pipelinestateobject
bool InitCamera();  //Init Camera
bool InitTexture(); //Init Texture
bool InitPerlinBuffer(); //Init Perlin buffer


//Variables:

// Handle to the window
HWND hwnd = NULL;
LPCTSTR WindowName = "Perlin Noise Terrain Demo";
LPCTSTR WindowTitle = "Perlin Noise Terrain Demo";

const int frameBufferCount = 3; // number of buffers we want, 2 for double buffering, 3 for tripple buffering
int Width = 2560;
int Height = 1440;
int nXVertices = 100;
int nYVertices = 100;
int gridSizeX = 99;
int gridSizeY = 99;
bool FullScreen = false;
bool Running = true;
int frameIndex; // current rtv we are on
int rtvDescriptorSize; // size of the rtv descriptor on the device (all front and back buffers will be the same size) function declarations

ID3D12Device* device; // direct3d device
IDXGISwapChain3* swapChain; // swapchain used to switch between render targets
ID3D12CommandQueue* commandQueue; // container for command lists
ID3D12DescriptorHeap* rtvDescriptorHeap; // a descriptor heap to hold resources like the render targets
ID3D12Resource* renderTargets[frameBufferCount]; // number of render targets equal to buffer count
ID3D12CommandAllocator* commandAllocator[frameBufferCount]; // we want enough allocators for each buffer * number of threads (we only have one thread)
ID3D12GraphicsCommandList* commandList; // a command list we can record commands into, then execute them to render the frame
ID3D12Fence* fence[frameBufferCount];    // an object that is locked while our command list is being executed by the gpu. We need as many as we have allocators (more if we want to know when the gpu is finished with an asset)
HANDLE fenceEvent; // a handle to an event when our fence is unlocked by the gpu
UINT64 fenceValue[frameBufferCount]; // this value is incremented each frame. each fence will have its own value

//Buffers:
ID3D12Resource* vertexBuffer;
ID3D12Resource* indexBuffer;
D3D12_VERTEX_BUFFER_VIEW vertexBufferView;
D3D12_INDEX_BUFFER_VIEW indexBufferView;

//Root Signature:
ID3D12RootSignature* rootSignature;
DXGI_SAMPLE_DESC sampleDesc;

//Pipeline Object
ID3D12PipelineState* pipelineStateObject;

//Viewport and Scissor Rect
D3D12_VIEWPORT viewport;
D3D12_RECT scissorRect;

//Camera Data
XMFLOAT4X4 cameraProjMat;
XMFLOAT4X4 cameraViewMat;

XMFLOAT4 cameraPosition; // this is our cameras position vector
XMFLOAT4 cameraTarget; // a vector describing the point in space our camera is looking at
XMFLOAT4 cameraUp; // the worlds up vector

ID3D12Resource* cameraBufferUploadHeaps[frameBufferCount];
CameraBuffer cameraData;
UINT8* cbvGPUAddress[frameBufferCount];

//DescriptorHeap
D3D12_DESCRIPTOR_HEAP_DESC heapDesc;
D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc;

//Texture Data
ID3D12Resource* textureBuffer; // the resource heap containing our texture
ID3D12DescriptorHeap* mainDescriptorHeap;
ID3D12Resource* textureBufferUploadHeap;

//Perlin Data
ID3D12Resource* perlinBuffer; // the resource heap containing our texture
ID3D12DescriptorHeap* perlinDescriptorHeap;
UINT8* perlinGPUAddress[frameBufferCount];
int mutationOffset;
float mutMod;
ID3D12Resource* perlinMutationBUHeaps[frameBufferCount];
UINT8* pmbGPUAddress[frameBufferCount];


//Depth Testing
ID3D12Resource* depthStencilBuffer; // This is the memory for our depth buffer. it will also be used for a stencil buffer in a later tutorial
ID3D12DescriptorHeap* dsDescriptorHeap; // This is a heap for our depth/stencil buffer descriptor

