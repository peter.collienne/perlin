Texture2D t1 : register(t0);
SamplerState s1 : register(s0);

struct VS_OUTPUT
{
	float4 pos: SV_POSITION;
	float3 normal: NORMAL; //In this case actually a blending parameter
	float2 texcoords: TEXCOORDS;
};

float4 main(VS_OUTPUT input) : SV_TARGET
{
	//Get texture data from sampler
	float4 color = t1.Sample(s1, input.texcoords * 10);

	//Three images are stored in the R,G,B channels. I misuse the normal parameter as blending factors!
	float4 mountainTex = float4(color.x, color.x, color.x, 0)*(input.normal.x);
	float4 grassTex = float4(color.y, color.y + 0.2f, color.y, 0)*(input.normal.y);
	float4 waterTex = float4(color.z, color.z, color.z + 0.2, 0)*(input.normal.z);
	float4 opacity = +float4(0, 0, 0, 1);

	return mountainTex + grassTex + waterTex + opacity;
}